const express = require("express");
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 10;
/*
const db = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'ebraille_sim'
});
*/
const db = mysql.createPool({
    host: 'sql11.freemysqlhosting.net',
    user: 'sql11487882',
    password: '4hD3mdBjcF',
    database: 'sql11487882'
});

//apply express middleware
app.use(express.json());

//apply bodyparser middleware
app.use(bodyParser.urlencoded({extended: true}))

//apply CORS
app.use(cors({
    origin: '*',
}));
app.options('*', cors());



/*User is trying to sign up*/
app.post("/api/signup", (req, res) => {

    res.set('Access-Control-Allow-Origin', '*');

    const email = req.body.email;
    const password = req.body.password;

    const checkEmail = "SELECT email FROM users WHERE email = ?";

    let emailAlredyTaken = false;

    db.query(checkEmail, [email], (err, result) => {
        console.log("email: "+ email);
        console.log("result: "+JSON.stringify(result));
        if (err) console.log(err);
        if(result.length>0) {
            emailAlredyTaken = true;
        }

        console.log("OK");
        if(emailAlredyTaken){
            //send error message
            res.send('This email is already taken');
        } else {

            bcrypt.hash(password, saltRounds, (err, hash) => {
                if (err) console.log(err);

                const sqlSignup = "INSERT INTO users (email, password) VALUES (?,?)";

                db.query(sqlSignup, [email, hash], (err, result) => {
                    console.log(result);
                    res.send('Success');
                });

            });

        }
    });
});

/*User is trying to login*/
app.post("/api/login", (req, res) => {

    res.set('Access-Control-Allow-Origin', '*');

    const email = req.body.email;
    const password = req.body.password;

    const login = "SELECT email, password FROM users WHERE email = ?";
    

    db.query(login, [email] , (err, result) => {
        console.log("email: "+ email);
        console.log("result: "+JSON.stringify(result));
        if (err) console.log(err);
        
        if(result.length>0) {
            //check hash password
            bcrypt.compare(password, result[0].password, (error, response) => {
                if(response){
                    res.send('Success');
                } else {
                    res.send('Wrong email/password combination');
                }
            });
        } else {
            res.send("User doesn't exist");
        }
    });
});

app.post('/api/getBookList', (req, res) => {

    res.set('Access-Control-Allow-Origin', '*');

    const email = req.body.email;

    if(email!='anon'){
        const getUserId = "SELECT id FROM users WHERE email = ?";
        db.query(getUserId, [email] , (err, result) => {
            console.log('GET user id:'+result[0].id);
            console.log("email: "+ email);
            console.log("result: "+JSON.stringify(result));
            if (err) console.log(err);

            if(result.length>0){
                let id = result[0].id;
                console.log("id: "+id);
                const sqlGetBookList = "SELECT id, title, author, genre, year FROM books WHERE user_id = ? OR user_id = 1";

                db.query(sqlGetBookList, [id],(err, result) => {
                    console.log("Final result: "+JSON.stringify(result));
                    
                    if (err) console.log(err);
                    res.send(result);
                });

            } else {
                res.send("Couldn't fetch user id from database");
            }

        });

    } else {
        //if user not logged, show only admin books
        const sqlGetBookList = "SELECT * FROM books WHERE user_id = 1";

        db.query(sqlGetBookList,(err, result) => {
            
            if (err) console.log(err);
            res.send(result);
        });
    }

})

app.post('/api/downloadBook', (req, res) => {

    res.set('Access-Control-Allow-Origin', '*');

    const bookId = req.body.book;
    console.log("id: "+bookId);

    const downloadBook = "SELECT book_file FROM books WHERE id = ?";

    db.query(downloadBook, [bookId],(err, result) => {
        console.log(result);
        if (err) console.log(err);
        res.send(result);
    });
});

app.post('/api/getUserUploadNumber', (req, res) => {

    res.set('Access-Control-Allow-Origin', '*');

    const email = req.body.email;
    
    let num=-1;
    //Check number of files uploaded by the user.
    const number = "SELECT u.id, b.user_id FROM users u, books b WHERE u.email = ? AND u.id=b.user_id";

    db.query(number, [email] , (err, result) => {
        console.log('UPLOAD NUMBER:');
        console.log("email: "+ email);
        console.log("result: "+JSON.stringify(result));
        if (err) console.log(err);

        num = result.length;
        console.log('Numero upload: '+num);

        if(num==-1 || num === undefined){
            res.send("Couldn't fetch information from database");
        } else {

            if (email=='admin') {
                res.send({info: 'Success', uploadNumber: 0});//If admin, always return 0 as response.uploadNumber
            } else {
                res.send({info: 'Success', uploadNumber: num});
            }
            
        }
        
    });

})

app.post('/api/uploadBook', (req, res) => {

    res.set('Access-Control-Allow-Origin', '*');

    const email = req.body.email;
    const title = req.body.title;
    const author = req.body.author;
    const genre = req.body.genre;
    const year = req.body.year;
    const file = req.body.file;

    //get user id by email
    //insert book to DB
    const getUserId = "SELECT id FROM users WHERE email = ?";
    db.query(getUserId, [email] , (err, result) => {
        console.log('GET user id:');
        console.log("email: "+ email);
        console.log("result: "+JSON.stringify(result));
        if (err) console.log(err);

        if(result.length>0){
            let id = result[0].id;
            //console.log("id: "+id);
            const insertBook = "INSERT INTO books (title, author, genre, year, user_id, book_file) VALUES (?,?,?,?,?,?)";

            db.query(insertBook, [title, author, genre, year, id, file], (err, result) => {
                
                if (err) console.log(err);

                res.send('Success');
            });

        } else {
            res.send("Couldn't fetch user id from database");
        }
    });

})

/* GET home page. */
app.get('/', function(req, res, next) {
    res.json({message: 'alive'});
  });

app.listen(3001, () => {
    console.log("running on port 3001");
});